package cn.edik.controller;

import com.jfinal.core.Controller;

/**
 * Author    : edik
 * CreateTime: 2019-02-13 16:53
 **/
public class IndexController extends Controller {

    public void index() {
        renderText("provider index");
    }
}
