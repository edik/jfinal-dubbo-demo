package cn.edik.common;

import cn.edik.controller.IndexController;
import cn.edik.plugin.dubbo.core.DubboPlugin;
import com.jfinal.config.*;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;

/**
 * Author    : edik
 * CreateTime: 2019-02-13 16:51
 **/
public class ProviderConfig extends JFinalConfig {

    /**
     * 启动入口，运行此 main 方法可以启动项目，此 main 方法可以放置在任意的 Class 类定义中，不一定要放于此
     */
    public static void main(String[] args) {
        UndertowServer.start(ProviderConfig.class);
    }

    @Override
    public void configConstant(Constants constants) {
        constants.setDevMode(true);
    }

    @Override
    public void configRoute(Routes routes) {
        routes.add("/", IndexController.class);
    }

    @Override
    public void configEngine(Engine engine) {

    }

    @Override
    public void configPlugin(Plugins plugins) {
        plugins.add(new DubboPlugin());
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {

    }

    @Override
    public void configHandler(Handlers handlers) {

    }

    @Override
    public void onStart() {
//        DubboServe dubboServe = new DubboServe();

//        dubboServe.init();
    }
}
