package cn.edik.dubbo;

import cn.edik.api.MyService;
import cn.edik.plugin.dubbo.annotation.Service;

/**
 * Author    : edik
 * CreateTime: 2019-02-15 17:00
 **/
@Service
public class MyServiceImpl implements MyService {
    @Override
    public String sayHello(String name) {
        return "Hello: " + name;
    }
}
