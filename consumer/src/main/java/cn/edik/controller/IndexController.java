package cn.edik.controller;

import cn.edik.api.MyService;
import cn.edik.plugin.dubbo.annotation.Reference;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.Para;

/**
 * Author    : edik
 * CreateTime: 2019-02-13 16:53
 **/
public class IndexController extends Controller {

    @Reference
    MyService myService;

    public void index(@Para(value = "name", defaultValue = "Edik") String name) {
        renderText(myService.sayHello(name));
    }
}
